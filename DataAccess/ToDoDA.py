import sqlite3
from tkinter import messagebox as msg

class ToDoDB:
    def __init__(self, task):
        self.Task = task

    def SaveTask(self):
        try:
            with sqlite3.Connection('DataAccess/todo.db') as db:
                query = f'''INSERT INTO "Tasks" ("Date" ,"Task") VALUES ("{self.Task[0]}", "{self.Task[1]}")'''
                Cursor = db.cursor()
                Cursor.execute(query)
                db.commit()
            
            msg.showinfo('Info','New task added successfully.')

        except:
            msg.showerror('ERROR','Something went WRONG!!')
