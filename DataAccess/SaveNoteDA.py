import sqlite3
from tkinter import filedialog

class NoteDB:
    def __init__(self, content):
        self.Content = content

    def SaveNewFile(self):
        try:
            with filedialog.asksaveasfile(initialfile= 'Untitled.txt',defaultextension=".txt",initialdir='/home/mohammad/Desktop',
                    filetypes=[("All Files","*.*"),("Text Documents","*.txt")]) as f:
                        f.write(self.Content)
        except:
            pass
    
    def SaveExistedFile(self,DIR):
            # with filedialog.asksaveasfilename(initialfile=DIR.split('/')[-1],initialdir=DIR) as f:
            with open(file=DIR, mode= 'w') as f:
                f.write(self.Content)
