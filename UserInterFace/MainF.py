import re
from tkinter import *
import sqlite3

class MainUI:
    def __init__(self, name):
        self.Name = name

    def MainFrom(self):
        main = Tk()
        main.title('Main Form')
        main.geometry("500x500")

        def NoteFunc():
            main.destroy()
            from UserInterFace import Note
            note = Note.NoteUI(self.Name)
            note.NoteForm()

        def CalenFunc():
            pass

        def NoteToDo():
            main.destroy()
            from UserInterFace import ToDoUI
            todo = ToDoUI(self.Name)
            todo.ToDoForm()
        
        Label(main, text="please select...", font='Helvetica 10').grid(row=0,column=0,padx=10,pady=10)

        btnNote = Button(main, text='notebook', font='Helvetica 9',relief='groove',command=NoteFunc)\
            .grid(row=1,column=0,padx=10,pady=10)

        btnCalen = Button(main, text='Calendar', font='Helvetica 9',relief='groove',command=CalenFunc)\
            .grid(row=1,column=1,padx=10,pady=10)

        btnToDo = Button(main, text='To-Do List', font='Helvetica 9',relief='groove',command=NoteToDo)\
            .grid(row=1,column=2,padx=10,pady=10)

        main.mainloop()