from ctypes import resize
from tkinter import *
from tkinter import ttk

class ToDoUI:
    def __init__(self, name):
        self.Name = name

    def ToDoForm(self):
        main = Tk()
        main.title('ToDo List')
        main.geometry('630x500')
        # main.resizable(0,0)

        #Functions
        def AddFunc():
            from UserInterFace import AddUI
            add = AddUI(self.Name)
            add.AddForm()

        def SaveFunc():
            pass

        def DelFunc():
            pass

        def BackFunc():
            main.destroy()
            from UserInterFace import MainF
            back = MainF.MainUI(self.Name)
            back.MainFrom()

        #Frames
        frame1 = Frame(main,bg='black',width=500)
        frame1.pack(side='left',expand=True, fill=BOTH)

        frame2 = Frame(main, width=60,bg='gray')
        frame2.pack(side='left',expand=True, fill=BOTH)

        #TreeView
        tv = ttk.Treeview(frame1, columns=('Check', 'Modified Date', 'Task'))
        tv.column('#0', width=0, stretch=NO)
        tv.column('Check', anchor=CENTER, width=5)
        tv.column('Modified Date', anchor=CENTER, width=20)
        tv.column('Task', anchor=CENTER, width=300)

        tv.heading('#0', text='', anchor=CENTER)
        tv.heading('Check', text='Check', anchor=CENTER)
        tv.heading('Modified Date', text='Date', anchor=CENTER)
        tv.heading('Task', text='Task', anchor=CENTER)

        tv.insert(parent='',index=0, values=('check', '3423', 'afdfafgag'))

        tv.pack(expand=True,fill=BOTH)


        #Buttons
        Button(frame2, text='    Add    ', font='Helvetica 10 bold', command=AddFunc, relief='raised')\
            .place(x=53,y=345)

        Button(frame2, text='    Save    ', font='Helvetica 10', command=SaveFunc, relief='raised')\
            .place(x=51,y=380)

        Button(frame2, text='   Delete   ', font='Helvetica 10', command=DelFunc, relief='raised')\
            .place(x=51,y=415)

        Button(frame2, text='    Back    ', font='Helvetica 10', command=BackFunc, relief='raised')\
            .place(x=51,y=450)

        main.mainloop()