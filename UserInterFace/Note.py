from tkinter import *
import sqlite3
from tkinter import messagebox as msg
from tkinter import filedialog

DIR = ''
first = ''
class NoteUI:
    def __init__(self, name):
        self.Name = name
        
    def NoteForm(self):
        main = Tk()
        main.title('NoteBook')
        main.geometry('600x600')

        def checkBack():
            if ent.get(1.0, "end-1c") != first:
                def gotoback():
                    second.destroy()
                    backFunc()
                second = Tk()
                second.title('Warning')
                second.geometry('350x90')
                second.resizable(0,0)
                Label(second, text="you didn't save your text.Do you wanna save?",font='Helvetica 10')\
                    .pack(padx=10,pady=10)
                Button(second, text='Save',font='Helvetica',relief='groove', command=saveInput)\
                    .pack(ipadx=10,padx=10,pady=10,expand=True,side='left')
                Button(second, text='No',font='Helvetica',relief='groove', command=gotoback)\
                    .pack(ipadx=10,padx=10,pady=10,expand=True,side='left')
                second.mainloop()

            else:
                backFunc()

        def backFunc():
            main.destroy()
            from UserInterFace import MainF
            back = MainF.MainUI(self.Name)
            back.MainFrom()

        def saveInput():
            inp = ent.get(1.0, "end-1c")
            from DataAccess import SaveNoteDA
            save = SaveNoteDA.NoteDB(inp)
            if DIR == '':
                save.SaveNewFile()
            else:
                save.SaveExistedFile(DIR)

        def openFunc():
            global DIR
            global first
            DIR = filedialog.askopenfilename(title='Select Text File',filetypes=[('Text File','*.txt')],
                initialdir='/home/mohammad/Desktop')
            if DIR != '':
                main.title(DIR.split('/')[-1])
                with open(file=DIR,mode='r') as file:
                    text = first = file.read()
                
                ent.delete(1.0,END)
                ent.insert(1.0, text)

        ent = Text(main)
        ent.pack(padx=5,pady=5,expand=True,fill=BOTH)
        ent.focus_set()
        Button(main, text='Back',font='Helvetica',relief='groove', command=checkBack)\
            .pack(ipadx=5,padx=10,pady=10,expand=True,side='left')
        Button(main, text='open',font='Helvetica',relief='groove', command=openFunc)\
            .pack(ipadx=5,padx=10,pady=10,expand=True,side='left')
        Button(main, text='Save',font='Helvetica',relief='groove', command=saveInput)\
            .pack(ipadx=5,padx=10,pady=10,expand=True,side='left')



        main.mainloop()