from numpy import pad
from pyparsing import col
import datetime
from tkinter import *


class AddUI:
    def __init__(self, name):
        self.Name = name


    def AddForm(self):
        new = Tk()
        new.title('Add Task')
        new.geometry('390x150')
        new.resizable(0,0)

        def cnclFunc():
            new.destroy()

        def addFunc():
            txtG = txtTask.get()
            date = datetime.date.today()
            Task = [txtG , date]
            from DataAccess import ToDoDB
            newT = ToDoDB(task=Task)
            newT.SaveTask()
            new.destroy()

        Label(new, text='Task: ', font='Helvetica 10').place(x=10,y=12)
        txtTask = StringVar(new)
        txt = Entry(new, width=40, font='Tahoma 10', highlightthickness=1, textvariable=txtTask)
        txt.place(x=70,y=10)
        txt.focus_set()


        Button(new, text='Cancel', font='Helvetica 10', relief='groove', command=cnclFunc)\
            .place(x=110,y=50)
        Button(new, text='  Add  ', font='Helvetica 10', relief='groove', command=addFunc)\
            .place(x=210,y=50)


        new.mainloop()